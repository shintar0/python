import turtle

t = turtle.Pen()

t.forward(50)
t.left(90)
t.forward(50)
t.left(90)
t.forward(50)
t.left(90)
t.forward(50)
t.left(90)

# t.reset()
t.clear()

t.reset()
t.backward(100)
t.up()
t.right(90)
t.forward(20)
t.left(90)
t.down()
t.forward(100)

# 三角形
t.reset()
t.forward(150)
t.left(120)
t.forward(150)
t.left(120)
t.forward(150)
t.left(120)

# 角なし四角形
t.reset()
t.forward(100)
t.up()
t.forward(20)
t.left(90)
t.forward(20)
t.down()
t.forward(100)
t.up()
t.forward(20)
t.left(90)
t.forward(20)
t.down()
t.forward(100)
t.up()
t.forward(20)
t.left(90)
t.forward(20)
t.down()
t.forward(100)
t.up()
t.forward(20)
t.left(90)
t.forward(20)
t.down()






input('press any key to exit ...')
