import os
print('getcwd:  ', os.getcwd())
print('__file__ ', __file__)

# file
test_file = open("C:\digi\pgm\work\private\python\\tanoshi_programing\\test.txt")
text = test_file.read()
print(text)
test_file.close()

test_file = open("C:\digi\pgm\work\private\python\\tanoshi_programing\\myfile.txt", 'w')
test_file.write('this is my test file')
test_file.close()

test_file = open("C:\digi\pgm\work\private\python\\tanoshi_programing\\myfile.txt", 'w')
test_file.write('What is green and loud? A froghorn!')
test_file.close()

test_file = open("C:\digi\pgm\work\private\python\\tanoshi_programing\\myfile.txt")
print(test_file.read())
test_file.close()
