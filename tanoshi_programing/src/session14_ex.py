from tkinter import *
import random
import time

# 13.3 ボールはクラス
class Ball:
    def __init__(self, canvas, paddle, color):
        self.canvas = canvas
        self.paddle = paddle
        self.id = canvas.create_oval(10, 10, 25, 25, fill=color)
        self.canvas.move(self.id, 245, 100)

        print(self.canvas.coords(self.id))

        starts = [-3, -2, -1, 1, 2, 3]
        random.shuffle(starts)
        self.x = starts[0]
        self.y = -1
        self.canvas_height = self.canvas.winfo_height()
        self.canvas_width = self.canvas.winfo_width()

        # 14.2 ゲームのルールは
        self.hit_bottom = False

        # 14.4 #4 いま、何点？
        self.point = 0
        canvas.create_text(400, 10, fill='blue', font='10px', text='your score: ')
        self.message_point = canvas.create_text(480, 10, fill='blue', font='10px', text=self.point)

    def draw(self):
        self.canvas.move(self.id, self.x, self.y)
        pos = self.canvas.coords(self.id)
        if pos[1] <= 0:
            self.y = 1
        if pos[3] >= self.canvas_height:
            # self.y = -1
            self.hit_bottom = True

        # paddleに当たったら
        if self.hit_paddle(pos):
            self.y = -3
            # paddleが速い場合は跳ね返りも速くする
            if self.paddle.x == -4 or self.paddle.x == 4:
                self.y = -15
            # 14.4 #4 いま、何点？
            self.point += 10
            canvas.itemconfig(self.message_point, text=self.point)

        if pos[0] <= 0:
            self.x = 3
        if pos[2] >= self.canvas_width:
            self.x = -3

    def hit_paddle(self, pos):
        paddle_pos = self.canvas.coords(self.paddle.id)
        if pos[2] >= paddle_pos[0] and pos[0] <= paddle_pos[2]:
            if pos[3] >= paddle_pos[1] and pos[3] <= paddle_pos[3]:
                return True
        return False

class Paddle:
    def __init__(self, canvas, color):
        self.canvas = canvas
        self.id = canvas.create_rectangle(0, 0, 100, 10, fill=color)
        self.canvas.move(self.id, 200, 300)
        self.x = 0
        self.canvas_width = self.canvas.winfo_width()
        self.canvas.bind_all('<KeyPress-Left>', self.turn_left)
        self.canvas.bind_all('<KeyPress-Right>', self.turn_right)

        # 14.4 #1 ゲームのスタートを遅らせる
        self.pause_game = True
        self.canvas.bind_all('<Button-1>', self.start_game)
    
    def draw(self):
        self.canvas.move(self.id, self.x, 0)
        pos = self.canvas.coords(self.id)
        if pos[0] <= 0:
            self.x = 0
        elif pos[2] >= self.canvas_width:
            self.x = 0
    
    def turn_left(self, evt):
        pos = self.canvas.coords(self.id)
        if pos[0] <= 0:
            self.x = 0
        else:
            # 既に-2の場合はもっと早くする
            if self.x == -2:
                self.x = -4
            else:
                self.x = -2
    def turn_right(self, evt):
        pos = self.canvas.coords(self.id)
        if pos[2] >= self.canvas_width:
            self.x = 0
        else:
            # 既に2の場合はもっと早くする
            if self.x == 2:
                self.x = 4
            else:
                self.x = 2
    
    def start_game(self, evt):
        self.pause_game = False


# 13.2 ゲームはキャンパスの上に
tk = Tk()
tk.title("Game")
tk.resizable(0, 0)
tk.wm_attributes("-topmost", 1)
canvas = Canvas(tk, width=500, height=400, bd=0, highlightthickness=0)
canvas.pack()
tk.update()


# 14.1 ラケットを作る
paddle = Paddle(canvas, 'blue')
# 13.3 ボールはクラス
ball = Ball(canvas, paddle, 'red')

while True:
    if ball.hit_bottom == False and paddle.pause_game == False:
        ball.draw()
        paddle.draw()
    # 14.4 #2 Game Over
    if ball.hit_bottom == True:
        message = canvas.create_text(250, 200, fill='red', font='20px', text='GAME OVER', state='hidden')
        time.sleep(1)
        canvas.itemconfig(message, state='normal')

    tk.update_idletasks()
    tk.update()
    time.sleep(0.01)




#input('press any key to exit ...')
