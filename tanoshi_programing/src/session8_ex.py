# 1.踊るキリン
class Things:
    pass
class Inanimate(Things):
    pass
class Animate(Things):
    pass
class Sidwalks(Inanimate):
    pass

class Animals(Animate):
    def breathe(self):
        print('breathing')
    def move(self):
        print('moving')
    def eat_food(self):
        print('eating food')

class Manmals(Animals):
    def feed_young_with_milk(self):
        print('feeding young')

class Giraffes(Manmals):
    def eat_leaves_from_trees(self):
        print('eating leaves')
        self.eat_food()
    def find_food(self):
        self.move()
        print("I've found food")
        self.eat_food()
    def dance_a_jig(self):
        print("dance a jig")
        self.move()
        self.move()
        self.move()
        self.move()
    def __init__(self, spots):
        self.giraffe_spots = spots
    def left_foot_forward(self):
        print('left foot forward')
    def left_foot_back(self):
        print('left foot back')
    def right_foot_forward(self):
        print('right foot forward')
    def right_foot_back(self):
        print('right foot back')
    def dance(self):
        self.left_foot_forward()
        self.left_foot_back()
        self.right_foot_forward()
        self.right_foot_back()
        self.left_foot_back()
        self.right_foot_back()
        self.right_foot_forward()
        self.left_foot_forward()

reginald = Giraffes(100)
reginald.dance()


# 2.干し草熊手
import turtle
avery1 = turtle.Pen()
avery2 = turtle.Pen()
avery3 = turtle.Pen()
avery4 = turtle.Pen()

avery1.forward(50)
avery1.left(90)
avery1.forward(40)
avery1.right(90)
avery1.forward(40)

avery2.forward(60)
avery2.left(90)
avery2.forward(20)
avery2.right(90)
avery2.forward(20)

avery3.forward(50)
avery3.right(90)
avery3.forward(40)
avery3.left(90)
avery3.forward(40)

avery4.forward(60)
avery4.right(90)
avery4.forward(20)
avery4.left(90)
avery4.forward(20)


input('press any key to exit ...')
