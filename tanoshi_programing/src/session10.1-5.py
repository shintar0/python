class Animal:
    def __init__(self, species, number_of_legs, color):
        self.species = species
        self.number_of_legs = number_of_legs
        self.color = color
    
harry = Animal('hippogriff', 6, 'pink')

import copy
harry = Animal('hippogriff', 6, 'pink')
harriet = copy.copy(harry)
print(harry.species)
print(harriet.species)

harry = Animal('hippogriff', 6, 'pink')
carrie = Animal('chimera', 4, 'green polka dots')
billy = Animal('bogill', 0, 'paisley')
my_animals = [harry, carrie, billy]
more_animals = copy.copy(my_animals)

print(my_animals)
print(more_animals)
print(more_animals[0].species)
print(more_animals[1].species)

my_animals[0].species = 'ghoul'
print(my_animals[0].species)
print(more_animals[0].species)

sally = Animal('sphinx', 4, 'sand')
my_animals.append(sally)
print(len(my_animals))
print(len(more_animals))

more_animals = copy.deepcopy(my_animals)
my_animals[0].species = 'wyrm'
print(my_animals[0].species)
print(more_animals[0].species)

import keyword
print(keyword.iskeyword('if'))
print(keyword.iskeyword('ozwald'))
print(keyword.kwlist)

import random
print(random.randint(1, 100))
print(random.randint(100, 1000))
print(random.randint(1000, 5000))

import random
num = random.randint(1, 100)
while True:
    print('Guess a number between 1 and 100')
    guess = input()
    i = int(guess)
    if i == num:
        print('You guessed right')
        break
    elif i < num:
        print(num)
        print('Try higher')
    elif i > num:
        print(num)
        print('Try lower')

import random
desserts = ['ice cream', 'pancakes', 'brownies', 'cookies', 'candy']
print(random.choice(desserts))

random.shuffle(desserts)
print(desserts)

import sys
# sys.exit()

v = sys.stdin.readline()
print(v)

v = sys.stdin.readline(5)
print(v)

import sys
sys.stdout.write("What does a fish say when it swims into a wall? Dam.")

print(sys.version)

import time
print(time.time())

def lots_of_numbers(max):
    t1 = time.time()
    for x in range(0, max):
        print(x)
    t2 = time.time()
    print('it took %s seconds' % (t2-t1))

lots_of_numbers(1000)

print(time.asctime())

t = (2007, 5, 27, 10, 30, 48, 6, 0, 0)
print(time.asctime(t))

print(time.localtime())

