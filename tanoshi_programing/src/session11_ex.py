import turtle
t = turtle.Pen()

def myoctagon(size, filled):
    if filled == True:
        t.begin_fill()
    for x in range(1, 9):
        t.forward(size)
        t.right(45)
    if filled == True:
        t.end_fill()

t.reset()
t.color(0.9, 0.75, 0)
myoctagon(60, True)
t.color(0, 0, 0)
myoctagon(60, False)

def draw_mystar(size, points):
    for x in range(1, points + 1):
        t.forward(100)
        if x % 2 == 0:
            t.left(175)
        else:
            t.left(225)


input('press any key to exit ...')


    
