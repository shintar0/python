import turtle
t = turtle.Pen()
t.reset()
t.color(1, 0, 0)
t.begin_fill()
t.forward(100)
t.left(90)
t.forward(20)
t.left(90)
t.forward(20)
t.right(90)
t.forward(20)
t.left(90)
t.forward(60)
t.left(90)
t.forward(20)
t.right(90)
t.forward(20)
t.left(90)
t.forward(20)
t.end_fill()

t.color(0,0,0)
t.up()
t.forward(10)
t.down()
t.begin_fill()
t.circle(10)
t.end_fill()

t.setheading(0)
t.up()
t.forward(90)
t.right(90)
t.forward(10)
t.setheading(0)
t.begin_fill()
t.down()
t.circle(10)
t.end_fill()

input('press any key to exit ...')

def mycircle(red, green, blue):
    t.reset()
    t.color(red, green, blue)
    t.begin_fill()
    t.circle(50)
    t.end_fill()

mycircle(0, 1, 0)
mycircle(0, 0.5, 0)

mycircle(1, 0, 0)
mycircle(0.5, 0, 0)
mycircle(0, 0, 1)
mycircle(0, 0, 0.5)

# gold
mycircle(0.9, 0.75, 0)

# pink
mycircle(1, 0.7, 0.75)

# orange
mycircle(1, 0.5, 0)
mycircle(0.9, 0.5, 0.15)

# black
mycircle(0,0,0)
# white
mycircle(1,1,1)

