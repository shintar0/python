for x in range(0, 5):
    print('hello')

print(list(range(10, 20)))

print(range(10, 20))

for x in range(0, 5):
    print('hello %s' % x)

wizard_list = ['spider legs', 'toe of frog', 'snail tongue',
              'bat wing', 'slug butter', 'bear burp']

for i in wizard_list:
    print(i)


for i in  ['spider legs', 'toe of frog', 'snail tongue']:
    print(i)

found_coins = 20
magic_coins = 70
stolen_coins = 3
coins = found_coins
for week in range(1, 53):
    coins = coins + magic_coins - stolen_coins
    print('Week %s = %s' % (week, coins))

tired = False
badweather = True
step = 0
while step < 10000:
    print(step)
    if tired == True:
        break
    elif badweather == True:
        break
    else:
        step = step + 1


x = 45
y = 80
while x < 50 and y < 100:
    x = x + 1
    y = y + 1
    print(x, y)

for x in range(0, 20):
    print('hello %s' % x)
    if x < 9:
        break

age = 14
for i in range(1, age + 1):
    if i % 2 == 0:
        print(i)

ingredients = ['snails', 'leeches', 'gorilla belly-button lint',
              'caterpillar eyebrows', 'centipede toes']

for i in ingredients:
    print(i)

for i in range(len(ingredients)):
    print('%s %s' % (i + 1, ingredients[i]))

weight = 65
for i in range(15):
    weight = weight + 1
    print('after %s year: %s kg' % (i + 1, weight * 0.165))






