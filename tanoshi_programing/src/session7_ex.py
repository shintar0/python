import sys

# def moon_weight(init, add, year):
def moon_weight():
    print('Please enter your current Earth weight')
    weight = int(sys.stdin.readline())
    print('Please enter the amount your weight might increase each year')
    add = float(sys.stdin.readline())
    print('Please enter the number of years')
    year = int(sys.stdin.readline())
    for i in range(year):
        weight = weight + add
        print('after %s year: %s kg' % (i + 1, weight * 0.165))

# moon_weight(30, 0.25)
# moon_weight(90, 0.25, 5)

moon_weight()
