class Things:
    pass
class Inanimate(Things):
    pass
class Animate(Things):
    pass
class Sidwalks(Inanimate):
    pass

class Animals(Animate):
    def breathe(self):
        print('breathing')
    def move(self):
        print('moving')
    def eat_food(self):
        print('eating food')

class Manmals(Animals):
    def feed_young_with_milk(self):
        print('feeding young')

class Giraffes(Manmals):
    def eat_leaves_from_trees(self):
        print('eating leaves')
        self.eat_food()
    def find_food(self):
        self.move()
        print("I've found food")
        self.eat_food()
    def dance_a_jig(self):
        print("dance a jig")
        self.move()
        self.move()
        self.move()
        self.move()
    def __init__(self, spots):
        self.giraffe_spots = spots
        

reginald = Giraffes(100)
reginald.move()
reginald.eat_leaves_from_trees()

harold = Giraffes(150)
harold.move()

harold.dance_a_jig()

print(reginald.giraffe_spots)
print(harold.giraffe_spots)

import turtle
avery = turtle.Pen()
kate = turtle.Pen()

avery.forward(50)
avery.right(90)
avery.forward(20)

kate.left(90)
kate.forward(100)

jacob = turtle.Pen()
jacob.left(180)
jacob.forward(80)

# kateを新規作成
kate = turtle.Pen()
kate.right(90)
kate.forward(100)
kate.left(90)
kate.forward(20)

avery.forward(100)
kate.forward(100)
jacob.forward(100)


input('press any key to exit ...')
