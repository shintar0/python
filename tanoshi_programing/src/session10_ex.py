# 1.コピーされた車
import copy
class Car:
    pass

car1 = Car()
car1.wheels = 4
car2 = car1
car2.wheels = 3
# 表示されるのは、3の想定
print(car1.wheels)

car3 = copy.copy(car1)
car3.wheels = 6
# 表示されるのは、3の想定
print(car1.wheels)

# 2.お気に入りリストを保存する
import pickle
fav_data = [
    'book',
    'pc',
    'sleep'
]

# save_file = open('fav_save.dat', 'wb')
# pickle.dump(fav_data, save_file)
# save_file.close()

load_file = open('fav_save.dat', 'rb')
loaded_data = pickle.load(load_file)
load_file.close()

print(loaded_data)
