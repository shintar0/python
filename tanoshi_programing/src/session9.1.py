print(abs(10))
print(abs(-10))

print()
print(bool(0))
print(bool(1))
print(bool(1123.23))
print(bool(-500))

print()
print(bool(None))
print(bool('a'))
print(bool(' '))
print(bool('What do you cal a pig doing karate? Pork Chop!'))

year = input('Year of birth: ')
# Enter then error
if not bool(year.rstrip()):
    print('You need to enter a value for your year of birth')

print(dir(['a']))

print(dir(['short', 'list']))

print(1)
print(dir(1))

popcorn = 'I love popcorn!'
print(popcorn)
print(dir(popcorn))

help(popcorn.upper)

# eval
your_calculation = input('Enter a caluculation:')
print(eval(your_calculation))

# exec
my_small_program = '''print('ham')
print('sandwich')'''
exec(my_small_program)

print(float('12'))
print(float('123.4567'))

your_age = input('Enter your age: ')
age = float(your_age)
if age > 13:
    print('You are %s years too old' % (age - 13))

print(int(123.456))
print(int('123'))

# len
# max min
# range
# sum

